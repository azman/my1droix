#!/bin/bash

[[ "$UID" -ne 0 ]] && echo "Must be root! Aborting!" && exit 1

THIS_PATH=$(cd $(dirname $0);pwd)

FILE_IMAGE=${FILE_IMAGE:="linux.img"}
LOOP_MOUNT=${LOOP_MOUNT:="loopfs"}

ROOTFS_NAME=${ROOTFS_NAME:="root-filesystem-armv5l"}
ROOTFS_FILE=${ROOTFS_FILE:="$ROOTFS_NAME.tar.bz2"}
ROOTFS_URL=${ROOTFS_URL:="http://landley.net/aboriginal/downloads/binaries/root-filesystem/"}

BUSYBX_NAME=${BUSYBX_NAME:="busybox-armv5l"}
BUSYBX_URL=${BUSYBX_URL:="http://busybox.net/downloads/binaries/latest/"}

DROPBR_NAME=${DROPBR_NAME:="dropbearmulti-armv5l"}
DROPBR_URL=${DROPBR_URL:="http://landley.net/aboriginal/downloads/binaries/extras/"}

GITBIN_BALL=${GITBIN_BALL:="git-static-armv5l.tar.gz"}
#GITBIN_URL=${GITBIN_URL:="self-built"}
RSYNC_BALL=${RSYNC_BALL:="rsync-static-armv5l.tar.gz"}
#RSYNC_URL=${RSYNC_URL:="self-built"}
VIMBIN_BALL=${VIMBIN_BALL:="vim-static-armv5l.tar.gz"}
#VIMBIN_URL=${VIMBIN_URL:="self-built"}

FILE_MDEVCONF=$(cat <<CHK_MDEVCONF
# /etc/mdev.conf

# Syntax:
# [dev_regex] [uid]:[gid] [perm] [=path|>path|!] [[@|$|*]<command>]
# [dev_regex] ->[symlink]
# more on http://git.busybox.net/busybox/plain/docs/mdev.txt

console 0:0 0600
null 0:0 0666
zero 0:0 0666
random 0:0 0666
urandom 0:0 0666
kmem 0:9 0640
mem 0:8 0640
tty 0:5 0666

sr0 0:19 1660 >cdrom
sr[0-9].* 0:19 1660 !
[h,s]d[a-z].* 0:6 0660 !
ram[0-3] 0:6 0660
ram[0-9].* 0:6 0660 !
loop[0-3] 0:6 0660
loop[0-9].* 0:6 0660 !
tty0 0:0 0640
tty[0-4] 0:0 0600
tty[0-9] 0:0 0660 !
tty[p,q][0-3] 0:5 0660
tty[p,q].* 0:5 0660 !
(ttyS)([0-3]) 0:16 0660 >com%2
ttyS.* 0:16 0660 !
(ttyUSB)([0-3]) 0:16 0660 >comUSB%2
ttyUSB.* 0:16 0660 !
tty.* 0:0 0660 !
pty[p,q][0-3] 0:5 0660
pty.* 0:5 0660 !
.* 0:0 0660 =unknown/
CHK_MDEVCONF
)

FILE_DBSSH=$(cat <<CHK_DBSSH
#!/bin/ash
dbclient -i ~/.ssh/id_rsa_db $@
CHK_DBSSH
)

function do_download()
{
	local fname="$1"
	local label="$2"
	[[ "$fname" == "" ]] && exit 1 # shouldn't be here?
	[[ "$label" == "" ]] && label="$fname"
	echo -n "Downloading $label:     "
	wget --progress=dot "$fname" 2>&1 | grep --line-buffered "%" | \
		sed -u -e "s,\.,,g" | awk '{printf("\b\b\b\b%4s", $2)}'
	echo -ne "\b\b\b\b DONE!\n"
}

DEBUG_LOG=${DEBUG_LOG:="image.log"}

function do_debuglog()
{
	echo -ne "$1"
	echo -ne "$1" >>$DEBUG_LOG
}

# check sources

[[ ! -f $ROOTFS_FILE ]] && do_download ${ROOTFS_URL}$ROOTFS_FILE $ROOTFS_FILE
[[ ! -f $BUSYBX_NAME ]] && do_download ${BUSYBX_URL}$BUSYBX_NAME $BUSYBX_NAME
[[ ! -f $DROPBR_NAME ]] && do_download ${DROPBR_URL}$DROPBR_NAME $DROPBR_NAME

# do that thing you do

[[ -f "$FILE_IMAGE" ]] && rm -rf $FILE_IMAGE
echo "" >$DEBUG_LOG
do_debuglog "Creating image file '$FILE_IMAGE'..."
dd if=/dev/zero of=$FILE_IMAGE bs=1024 count=500K >>$DEBUG_LOG 2>&1
do_debuglog " done!\n"
do_debuglog "Formatting image file '$FILE_IMAGE'..."
mke2fs -F $FILE_IMAGE >>$DEBUG_LOG 2>&1
tune2fs -c 0 -i 0 $FILE_IMAGE >>$DEBUG_LOG
do_debuglog " done!\n"
REMOVE_MOUNT="NO"
[[ ! -d $LOOP_MOUNT ]] && REMOVE_MOUNT="YES"
mkdir -p $LOOP_MOUNT
mount -o loop $FILE_IMAGE $LOOP_MOUNT
do_debuglog "Extracting root fs from '$ROOTFS_FILE'..."
rm -rf $ROOTFS_NAME
tar -xvf $ROOTFS_FILE >>$DEBUG_LOG
cp -a $ROOTFS_NAME/* $LOOP_MOUNT/
rm -rf $ROOTFS_NAME
do_debuglog " done!\n"
do_debuglog "Creating mount-point for sdcard..."
mkdir -p $LOOP_MOUNT/mnt/sdcard
do_debuglog " done!\n"
chown root:root -R $LOOP_MOUNT/*
do_debuglog "Copying useful static binaries into image file..."
cp $BUSYBX_NAME $LOOP_MOUNT/bin/busybox
chmod 755 $LOOP_MOUNT/bin/busybox
cp $DROPBR_NAME $LOOP_MOUNT/bin/dropbear
ln -sf dropbear $LOOP_MOUNT/bin/dbclient
ln -sf dropbear $LOOP_MOUNT/bin/dropbearkey
ln -sf dropbear $LOOP_MOUNT/bin/dropbearconvert
ln -sf dropbear $LOOP_MOUNT/bin/scp
echo "$FILE_DBSSH" > $LOOP_MOUNT/bin/ssh
chmod 755 $LOOP_MOUNT/bin/dropbear
chmod 755 $LOOP_MOUNT/bin/ssh
do_debuglog " done!\n"
[[ -f $GITBIN_BALL ]] && do_debuglog "Extracting git binary..." &&
	tar -C $LOOP_MOUNT/ -xzf $GITBIN_BALL >>$DEBUG_LOG && do_debuglog " done!\n"
[[ -f $RSYNC_BALL ]] && do_debuglog "Extracting rsync binary..." &&
	tar -C $LOOP_MOUNT/ -xzf $RSYNC_BALL >>$DEBUG_LOG && do_debuglog " done!\n"
[[ -f $VIMBIN_BALL ]] && do_debuglog "Extracting vim binary..." &&
	tar -C $LOOP_MOUNT/ -xzf $VIMBIN_BALL >>$DEBUG_LOG && do_debuglog " done!\n"
do_debuglog "Copying useful config file into image file..."
echo "$FILE_MDEVCONF" > $LOOP_MOUNT/etc/mdev.conf
echo "127.0.0.1 localhost" > $LOOP_MOUNT/etc/hosts
# basic settings - using google public dns ipv4 address
echo "nameserver 8.8.8.8" > $LOOP_MOUNT/etc/resolv.conf
echo "nameserver 8.8.4.4" >> $LOOP_MOUNT/etc/resolv.conf
do_debuglog " done!\n"
umount $LOOP_MOUNT
[[ "$REMOVE_MOUNT" == "YES" ]] && rm -rf $LOOP_MOUNT
# save info of this file's owner
USERSNAME=$(echo "$(ls -l $THIS_PATH)" | grep "$(basename $0)")
GROUPNAME=${USERSNAME#* * }
USERSNAME=${GROUPNAME%% *}
GROUPNAME=${GROUPNAME#${USERSNAME} }
GROUPNAME=${GROUPNAME%% *}
#echo "User: $USERSNAME, Group: $GROUPNAME."
md5sum $FILE_IMAGE >${FILE_IMAGE}.md5
chown $USERSNAME:$GROUPNAME ${FILE_IMAGE}*
chown $USERSNAME:$GROUPNAME $DEBUG_LOG

exit 0

# compile git with:
# ./configure --prefix=/usr/local --with-python= --without-tcltk CFLAGS="${CFLAGS} -static" NO_GETTEXT=1
# compile rsync with:
# ./configure CFLAGS="${CFLAGS} -static"
# compile vim with:
# LDFLAGS="-static" ./configure --without-x --disable-nls --disable-gui --disable-gpm --with-features=huge
